<?hh //partial
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\database\queryBuilder\driver\mongo
{
	use nuclio\core\
	{
		ClassManager,
		plugin\Plugin
	};
	use nuclio\plugin\database\
	{
		exception\DatabaseException,
		queryBuilder\CommonQueryBuilderInterface,
		datasource\source\Source,
		orm\Model,
		common\CommonCursorInterface,
		queryBuilder\ExecutionParams
	};
	
	<<provides('queryBuilder::mongo')>>
	class Mongo extends Plugin implements CommonQueryBuilderInterface
	{
		private Source $source;
		
		public static function getInstance(Source $source):Mongo
		{
			$instance=ClassManager::getClassInstance(self::class,$source);
			return ($instance instanceof self)?$instance:new self($source);
		}
		
		public function __construct(Source $source)
		{
			parent::__construct();
			$this->source=$source;
		}
		
		public function execute(ExecutionParams $params):CommonCursorInterface
		{
			$params=new Map((array)$params);
			$target	=$params->get('target');
			$filter	=$params->get('filter');
			$limit	=$params->get('limit');
			$offset	=$params->get('offset');
			$orderBy=$params->get('orderBy');
			$joins	=$params->get('joins');
			
			if (is_null($target))
			{
				throw new DatabaseException('Unable to execute query. Missing target parameter.');
			}
			if (is_null($filter))
			{
				throw new DatabaseException('Unable to execute query. Missing filter parameter.');
			}
			$cursor=$this->source->find($target,$filter);
			if ($limit)
			{
				$cursor->limit($limit);
			}
			if ($offset)
			{
				$cursor->offset($offset);
			}
			if ($orderBy)
			{
				$cursor->orderBy($orderBy);
			}
			return $cursor;
		}
		
		// public function parse(Map<string,mixed> $query):Map<string,mixed>
		// {
			
		// }
	}
}
